//
// Created by simo on 4.8.2015.
//

#include <iostream>
#include "Print.h"

Print::Print(int x, int y) : mX(x), mY(y) { }

void Print::print() {
    std::cout << mX << " " << mY << std::endl;
}