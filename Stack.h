//
// Created by simo on 4.8.2015.
//

#ifndef LEARNINGC___HUMANSTACK_H
#define LEARNINGC___HUMANSTACK_H

/*-------------------------------------------------------
                    <copyright notice>

Determine if two types are the same. Example:

template <typename T, typename U>
void do_something(const T&, const U&, bool flag);

template <typename T, typename U>
void do_something(const T& t, const U& u)
{
    do_something(t, u, is_same<T,U>::value);
}

---------------------------------------------------------*/
class HumanStack {


public:
    HumanStack();
//    HumanStack(const HumanStack & obj);
//    HumanStack & operator=(const HumanStack & obj);
//    ~HumanStack();
    int size();
    bool isEmpty() const;
    void clear();
    void push(int value);
    int * pop();
    int peek() const;
    int size() const;
    bool isFull() const;
private:
//    const int mSize = 10;//equivalent for java final member data
    int mArray[10];
    int mPosition;
    const int mSize = 10;
    void underflow() const;
};


#endif //LEARNINGC___HUMANSTACK_H
