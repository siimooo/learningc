//
// Created by simo on 4.8.2015.
//

#ifndef LEARNINGC___LISTNODE_H
#define LEARNINGC___LISTNODE_H

#include <stddef.h>

class ListNode {
public:
    int mElement;
    ListNode* mNext;
    ListNode(int theElement, ListNode * n = NULL);
};


#endif //LEARNINGC___LISTNODE_H
