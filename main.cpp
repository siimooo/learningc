#include <iostream>
#include <climits>
#include "min2.h"
#include "Human.h"
#include "ListNode.h"
#include "Print.h"
#include "Stack.h"
#include "LinkedStack.h"
#include <vector>

using namespace std;

void swap(int& x, int& y){
    int temp = x;
    x = y;
    y = temp;
    cout << x << y << endl;
}

void referenceR(const int & x){//read-only reference of x, non null
    cout << "referenceR " << x << endl;
}

void referenceC(int & x){//can change reference of x, non null
    x = 5;
    cout << "referenceC " << x << endl;
}

void referenceN(int x){//do nothing to client app, non null
    x = 50;
    cout << "referenceN " << x << endl;
}

void pointerR(const int * ptr){//read-only pointer can be null (handle it)
    cout << "pointerR " << *ptr << endl;
}

void pointerC(int * ptr){//can change pointer value can be null (handle it)
    *ptr = 43;
    cout << "pointerC " << *ptr << endl;
}

void print_list(ListNode* traverse)
{
    if (traverse == NULL) return;
    print_list(traverse->mNext);
    std::cout << traverse->mElement;
}

int main() {

//    vector<int> array1(1);//allowed 0 and 1 index
//    vector<int> array2;
//    array1.push_back(3);
//    array1.push_back(4);
//    array1.push_back(5);

//    cout << longString.length() << endl;
//    cout << "min is " << min2(6, 5) << endl;

//    int x = 2;
//    int y = 4;
//    wrongSwap(x,y);
//    cout << x << y << endl;
//    swap(x,y);
//    cout << x << y << endl;

//    int x = 5;
//    int y = 2;
//
//    int* pointer = NULL;

    //reference to x;
//    pointer = &x;
    //    *pointer += 10;
    //    int* pointer = &y;
    //    *pointer = 10;

//    *pointer = 0;
//    *pointer += 1; difference to
//    *pointer++; this

//    cout << *pointer << endl;
//    int j = 5;
//    int* ptr1 = NULL;
//    ptr1 = &j;
//
//    int* ptr2 = NULL;
//    ptr2 = &j;

//    *ptr1 += 1;
//    *ptr2++;
//
//
//    cout << "ptr1 " << *ptr1 << " ptr2 " << *ptr2 << endl;


//    int* result = changePointer(4);
//    cout << *result << endl;
//    delete result;

//    int x = 1;
//    int y = 5;
//
//    int* ptr1 = &x;
//    cout << **&ptr1 << endl;

//    Human piia("Piia");
//    Human copyOfPiia = piia;
//    copyOfPiia.setName("Pauliina");
//    cout << piia.getName() <<  " " << copyOfPiia.getName() << endl;
////
//////    int x = 5;
//////    int* ptr1 = &x;
//////    cout << *ptr1 << endl;
//////    int y = 7;
//////    ptr1 = &y;
//////
//////    cout << *ptr1 << endl;
//////    int x = 5;
//////    int* ptr1 = &x;//point to value x
//////    int* ptr2 = new int;//unvalued ptr2
//    int x = 3;
//    referenceR(x);
//    referenceC(x);
//    referenceN(x);
//    cout << "Client " << x << endl;
//
//    int* ptr1 = &x;
//    pointerR(ptr1);
//    pointerC(ptr1);
//    cout << "Client "<< *ptr1 << endl;
//
//    Print print;
//    print.print();
//    print.mY = 3;
//    print.print();
//
//    Print another = print;
//    another.print();
//    another.mY = 5;
//    another.print();
//    print.print();
//
//    HumanStack stack;
//    stack.push(3);
//    stack.push(56);
//    stack.push(3);
//    stack.push(56);
//    stack.push(3);
//    stack.push(56);
//    stack.push(3);
//    stack.push(56);
//    stack.push(10);
//    stack.push(1);
//    HumanStack copy = stack;
//    stack.clear();
//
//    int* pop = copy.pop();
//    cout << "Pop " << *pop << endl;
//    delete pop;
//    cout << "Peek " << copy.peek() << ", size " << copy.size() << endl;
//
//    LinkedStack stack2;
//    stack2.push(3);
//
//    LinkedStack copy2 = stack2;
//    cout << copy2.peek() << " " << copy2.size() << endl;

//
//    ListNode* ptr6 = new ListNode(2, new ListNode(9, NULL));
//    ListNode* ptr5 = new ListNode(5, NULL);
//    ptr5->mNext = ptr6;
//    ptr6 = ptr5;
//
//    cout << ptr5->mNext->mElement << " " << ptr6->mElement << endl;
//    ListNode* old = ptr6;
//    ptr6 = ptr6->mNext;
//    delete old;
//    cout << ptr5->mElement << " " << ptr6->mElement << endl;


//    ListNode* ptr6 = new ListNode(2, new ListNode(9, NULL));
//    ListNode* ptr5 = new ListNode(5);
//
//    ptr5->mNext = ptr6;
//    ptr6 = ptr5;
//    cout << ptr5->mElement << " " << ptr6->mElement << endl;
//
//    ListNode* old = ptr6;
//    ptr6 = ptr6->mNext;
//    delete old;
//    cout << ptr5->mElement << " " << ptr6->mElement << endl;

    LinkedStack stack3;
    stack3.push(5);
    stack3.push(6);
    stack3.push(7);
    cout << stack3.peek() << endl;

    LinkedStack copy3;
    copy3 = stack3;
    cout << copy3.size() << " " << copy3.peek() << endl;


//    ListNode* first = new ListNode(3);
//    ListNode* last = new ListNode(5, NULL);
//
//    cout << first->mElement << " " << last->mElement << endl;
//    first->mNext = last;
//
//    cout << first->mNext->mElement << " " << last->mElement << endl;
//
//
//    ListNode* node = new ListNode(2);
//    node->mNext = first;
//    first = node;
//
//
//
//    print_list(first);

//
//
//
//    cout << first->mElement << " " << last->mElement << endl;



    return 0;
}

