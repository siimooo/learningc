//
// Created by simo on 2.8.2015.
//

#include <iostream>
#include "Human.h"

Human::Human(std::string name) {//simple
    mName = new std::string(name);
    //or
    //mName = new std::string();
    //*mName = name;
}

Human::Human(const Human &obj) {//copy
    mName = new std::string(*obj.mName);
    //or
    //mName = new std::string();
    //*nName = *obj.mName; -> *obj.mName get value of name (not memory address)
}

Human& Human::operator=(const Human &obj) {//operator= ->copy assignment operator
    std::cout << "Y " << std::endl;
    if(this != &obj){
        *mName = *obj.mName;
    }
    return *this;
}

Human::~Human() {//destructor
    delete mName;
}

std::string Human::getName() const{
    return *mName;
}

void Human::setName(std::string name) {
    *mName = name;
}