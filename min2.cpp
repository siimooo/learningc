//
// Created by simo on 28.7.2015.
//

#include "min2.h"

int min2(int x, int y){
    return x < y ? x : y;
}
