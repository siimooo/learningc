//
// Created by simo on 5.8.2015.
//

#include <iostream>
#include "LinkedStack.h"
#include "ListNode.h"

LinkedStack::ListNode::ListNode(int value, ListNode * node) :  mValue(value), mNext(node) { } //list-initializer

LinkedStack::LinkedStack() : mSize(0), mFront(NULL) { } //list-initializer

LinkedStack::~LinkedStack() {
    clear();
}

LinkedStack& LinkedStack::operator=(const LinkedStack &obj) {
    if(this != &obj){
        // Step 1: Free current memory
        clear();

      //Step 2: Create a copy of rhs
        ListNode* temp = obj.mFront;
        reverse(temp);
    }
    return *this;
}

void LinkedStack::reverse(ListNode *node) {
    if(node == NULL){
        return;
    }
    reverse(node->mNext);
    push(node->mValue);
}

LinkedStack::LinkedStack(const LinkedStack &obj) { //copy
    mSize = 0;
    ListNode* temp = obj.mFront;
    reverse(temp);
}

void LinkedStack::clear() {
    while(!isEmpty()){
        pop();
    }
}

int LinkedStack::pop() {
    underflow();

    ListNode* old = mFront;
    int value = old->mValue;
    mFront = mFront->mNext;
    delete old;
    mSize--;

    return value;
}

int LinkedStack::peek() const {
    underflow();
    return mFront->mValue;
}

void LinkedStack::push(int value) {
    if(isFull()){
        std::cout << "stack is full! " << std::endl;
        return;
    }

    if(isEmpty()){//special case
        mSize++;
        mFront = new ListNode(value, NULL);
        return;
    }

    mSize++;
    ListNode* node = new ListNode(value);
    node->mNext = mFront;
    mFront = node;
}

void LinkedStack::underflow() const {
    if(isEmpty()){
        throw UnderflowException();
    }
}

int LinkedStack::size() {
    return mSize;
}

bool LinkedStack::isFull() const {
    return mSize >= mMax;
}

bool LinkedStack::isEmpty() const {
    return mSize <= 0;
}