//
// Created by simo on 2.8.2015.
//

#ifndef LEARNINGC___HUMAN_H
#define LEARNINGC___HUMAN_H

#include "string"
//proper deep copy & pointer member handling
class Human {
public:
    explicit Human(std::string name);// simple constructor //notice: explicit avoid wrong object type casting
    Human(const Human & obj);//copy-constructor
    ~Human();//destructor
    Human & operator=(const Human & obj);//operator=
    std::string getName() const;//accessor
    void setName(std::string name);//mutaitor
private:
    std::string* mName; //pointer member variable
};


#endif //LEARNINGC___HUMAN_H
