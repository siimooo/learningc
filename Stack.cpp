//
// Created by simo on 4.8.2015.
//

#include <iostream>
#include "Stack.h"

class UnderflowException { };

HumanStack::HumanStack() : mPosition(0){ }//initialize array to zero?

void HumanStack::clear() {
    while(!isEmpty()){
        int* popPtr = pop();
        delete popPtr;
    }
}

int HumanStack::size() {
    return mPosition + 1;
}

/**
 * Return pointer of array last index. Must call delete!
 */
int * HumanStack::pop() {
    underflow();
    int* ptr = new int(mArray[mPosition]);
    mArray[mPosition] = 0;
    mPosition--;
    return ptr;
}

int HumanStack::peek() const {
    underflow();
    return mArray[mPosition];
}

void HumanStack::push(int item) {
    if(isFull()){
        std::cout << "Stack is full" << std::endl;
        return;
    }
    mArray[++mPosition] = item;
}

bool HumanStack::isEmpty() const{
    return mPosition == 0;
}

bool HumanStack::isFull() const {
    return (mSize - 1) <= mPosition;
}

void HumanStack::underflow() const {
    if(isEmpty()){
        throw UnderflowException();
    }
}