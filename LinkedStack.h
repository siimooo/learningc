//
// Created by simo on 5.8.2015.
//

#ifndef LEARNINGC___LINKEDSTACK_H
#define LEARNINGC___LINKEDSTACK_H


#include <stddef.h>
#include <limits.h>

class LinkedStack {
public:
    LinkedStack();
    LinkedStack & operator=(const LinkedStack & obj); //operator=
    ~LinkedStack();//destructor
    LinkedStack(const LinkedStack & obj);//copy
    int size();
    bool isEmpty() const;
    void clear();
    void push(int value);
    int  pop();
    int peek() const;
    int size() const;
    bool isFull() const;
private:
    struct ListNode{ // nested-struct / class
            ListNode* mNext;
            int mValue;
            ListNode(int value, ListNode * node = NULL);
    };

    class UnderflowException{ };

    void underflow() const;

    ListNode* mFront;

    /**
     * Use recursion to keep order of stack when copying
     */
    void reverse(ListNode* node);
    int mSize;
    const int mMax = INT_MAX;
};


#endif //LEARNINGC___LINKEDSTACK_H
